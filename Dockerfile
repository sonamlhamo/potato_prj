# Use an appropriate base image that supports CUDA
FROM nvidia/cuda:11.8.0-cudnn8-runtime-ubuntu20.04

# Set environment variables for TensorRT
ENV LD_LIBRARY_PATH=/usr/local/cuda/lib64:/usr/local/tensorrt/lib:/usr/local/lib64

WORKDIR /app

# Copy requirements.txt to the working directory
COPY requirements.txt .

# Install dependencies
RUN apt-get update && apt-get install -y \
    libgl1-mesa-glx \
    libglib2.0-0 \
    python3-pip \
    python3-dev \
    wget \
    && rm -rf /var/lib/apt/lists/*

# Upgrade pip
RUN pip install --upgrade pip

# Install Python dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Download and install TensorRT
RUN wget https://developer.download.nvidia.com/compute/machine-learning/tensorrt/secure/8.6.1/local_repos/nv-tensorrt-repo-ubuntu2004-cuda11.8-trt8.6.1.6-e8e11bb8_1-1_amd64.deb \
    && dpkg -i nv-tensorrt-repo-ubuntu2004-cuda11.8-trt8.6.1.6-e8e11bb8_1-1_amd64.deb \
    && apt-key add /var/nv-tensorrt-repo-cuda11.8-trt8.6.1.6-e8e11bb8/7fa2af80.pub \
    && apt-get update \
    && apt-get install -y tensorrt \
    && apt-get install -y python3-libnvinfer \
    && rm -rf /var/lib/apt/lists/*

# Copy the rest of the application code to the working directory
COPY . .

# Expose port 5000 to allow external access
EXPOSE 5000

# Specify the command to run the application
CMD ["python", "app.py"]
